﻿using FormationCSharpInterm.Aggregation.Faible;
using FormationCSharpInterm.Aggregation.Forte;
using FormationCSharpInterm.DesignPatterns.Comportement.ChainOfResponsibility;
using FormationCSharpInterm.DesignPatterns.Comportement.Command;
using FormationCSharpInterm.DesignPatterns.Comportement.Observer;
using FormationCSharpInterm.DesignPatterns.Structure.Adapter;
using FormationCSharpInterm.DesignPatterns.Structure.Decorator;
using FormationCSharpInterm.DesignPatterns.Structure.Proxy;
using FormationCSharpInterm.Encapsulation;
using FormationCSharpInterm.EntitesVsObjetsValeurs;
using FormationCSharpInterm.Genericite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using static FormationCSharpInterm.DesignPatterns.Comportement.ChainOfResponsibility.Plainte;
using ContactObserver = FormationCSharpInterm.DesignPatterns.Comportement.Observer.Contact;

namespace FormationCSharpInterm
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Bonjour le terminal !");

            #region Genericite
            /*IDao<Produit> pDao = new ProduitDao();
            List<Produit> produits = pDao.RecupererTout();
            pDao.Inserer(new Produit());*/

            var genClass = new GenClass<int>
            {
                A = 12,
                B = 45
            };

            genClass.Swap();

            Console.WriteLine(genClass.A);
            #endregion

            #region Lambda
            Thread th = new Thread(() =>
            {
                Console.WriteLine("Thread 1");
            });
            th.Start();


            List<Produit> list = new List<Produit>();

            list.Sort((a, b) =>
            {
                if (a.Prix == b.Prix) return 0;
                else if (a.Prix < b.Prix) return -1;
                else return 1;
            });

            list.ForEach(p => Console.WriteLine(p.Description));

            var listeTriee = list.OrderByDescending(p => p.Prix);


            //    x1       x2    return
            Func<double, double, double> pow = (x1, x2) => Math.Pow(x1, x2);
            var resP = pow(23, 5);
            #endregion

            #region Aggregation
            var pCinema = new Parking();
            var diesel = new Moteur("diesel");
            var v = new Voiture(diesel);

            v.Garer(pCinema);

            var client = new Client
            {
                Nom = "DUPOND",
                Prenom = "Fred",
            };

            client.Adresse = new Adresse { CodePostal = 42300, NumRue = 17, Ville = "Strasbourg" };
            #endregion

            #region Encapsulation
            var r = new Rectangle(28, 30);
            var aire = r.Aire;
            r.Redim(50, 70);
            #endregion

            #region EntitesVsObjetsValeurs
            var e1 = new Entreprise(1, "Dawan", new Geolocation(48.8543, 2.3527));
            var e2 = new Entreprise(2, "Jehann", new Geolocation(48.8543, 3.37));
            int compParNom = e1.CompareTo(e2);
            int compParLocalisation = e1.ComparerParLocalisation(e2);

            List<Entreprise> entrepriseList = new List<Entreprise>() { e2, e1 };
            entrepriseList.Sort(); //tri par nom
            entrepriseList.ForEach(e => Console.WriteLine(e.Nom));

            entrepriseList.Sort((a, b) => a.ComparerParLocalisation(b)); //tri par distance
            entrepriseList.ForEach(e => Console.WriteLine(e.Nom + " " + e.Localisation));
            #endregion

            #region Design Patterns - Adapter
            var xmlRepo = new ContactRepository();

            IJsonAdapter jsonAdapter = new JsonAdapter(xmlRepo);
            /*string json = jsonAdapter.RecupererContactsJson("contacts.xml");
            Console.WriteLine(json);*/
            #endregion

            #region Design Patterns - Decorator
            /*IVoiture voiture = new VoitureBasique();
            voiture.Assembler();

            voiture = new SportDecorator(voiture);
            voiture.Assembler();

            voiture = new LuxeDecorator(voiture);
            voiture.Assembler();*/

            Console.WriteLine("Autre solution______________");
            IVoiture voitureSportLuxe = new SportDecorator(new LuxeDecorator(new VoitureBasique()));
            voitureSportLuxe.Assembler();
            #endregion

            #region Design Patterns - Proxy
            IMessage message = new ProxyMessage(new MessageUtilisateur("bonjour"));
            Console.WriteLine(message.RecupererContenu());
            #endregion

            #region Design Patterns - Chain Of Responsibility
            MembreEquipe formateur = new Formateur("Florian", new DirPeda("Marie-Claire", new Directeur("Jérôme", null)));

            Console.WriteLine("--REQ 1--");
            formateur.Handle(new Plainte(123, 1, "req1", EtatPlainte.Ouvert));

            Console.WriteLine("--REQ 2--");
            formateur.Handle(new Plainte(124, 2, "req2", EtatPlainte.Ouvert));

            Console.WriteLine("--REQ 3--");
            formateur.Handle(new Plainte(123, 3, "req3", EtatPlainte.Ouvert));
            #endregion

            #region Design Patterns - Observer
            Article article = new Article { Description = "RTX 4090", Prix = 2000 };

            IDisposable disC1 = article.Subscribe(new ContactObserver { Nom = "Florian" });
            IDisposable disC2 = article.Subscribe(new ContactObserver { Nom = "Benjamin" });

            article.Prix = 1200; //le changement de prix déclenche les notifications

            disC1.Dispose(); //désabonnement de C1

            article.Prix = 1050;
            #endregion

            #region Design Patterns - Command
            CookingStaffReceiver staff = new CookingStaffReceiver();
            WaiterInvoker waiter = new WaiterInvoker();

            var order = new LunchOrder("Faux-filet", staff);
            waiter.TakeOrder(order);
            #endregion

            Console.ReadLine();
        }
    }
}
