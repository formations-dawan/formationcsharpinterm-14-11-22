﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterm.DesignPatterns.Structure.Adapter
{
    internal class JsonAdapter : IJsonAdapter
    {
        private IContactRepository _xmlRepository;

        public JsonAdapter(IContactRepository xmlRepository)
        {
            _xmlRepository = xmlRepository;
        }

        string IJsonAdapter.RecupererContactsJson(string cheminFichier)
        {
            string xml = _xmlRepository.RecupererContactsXml(cheminFichier);

            //Transformation en JSON
            var contacts = _xmlRepository.DepuisXml(xml);

            var serializer = new DataContractJsonSerializer(contacts.GetType());

            string json;
            using (var stream = new MemoryStream())
            {
                serializer.WriteObject(stream, contacts);
                json = Encoding.UTF8.GetString(stream.ToArray());
            }
            return json;
        }
    }
}
