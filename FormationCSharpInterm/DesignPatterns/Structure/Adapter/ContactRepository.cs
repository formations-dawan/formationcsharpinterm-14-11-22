﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace FormationCSharpInterm.DesignPatterns.Structure.Adapter
{
    internal class ContactRepository : IContactRepository
    {
        List<Contact> IContactRepository.DepuisXml(string xml)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);

            XmlNodeList noeuds = doc.DocumentElement.SelectNodes("/Contacts/Contact");
            List<Contact> contacts = new List<Contact>();

            foreach (XmlNode noeud in noeuds)
            {
                var c = new Contact()
                {
                    Id = Convert.ToInt32(noeud.Attributes["id"].Value),
                    Nom = noeud.ChildNodes[0].InnerText
                };

                contacts.Add(c);
            }

            return contacts;
        }

        string IContactRepository.RecupererContactsXml(string cheminFichier)
        {
            string xml = null;

            using (var str = new StreamReader(cheminFichier))
            {
                xml = str.ReadToEnd();
            }

            return xml;
        }
    }
}
