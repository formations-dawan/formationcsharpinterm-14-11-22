﻿using System.Runtime.Serialization;

namespace FormationCSharpInterm.DesignPatterns.Structure.Adapter
{
    [DataContract]
    public class Contact
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Nom { get; set; }
    }
}