﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterm.DesignPatterns.Structure.Decorator
{
    internal class VoitureBasique : IVoiture
    {
        void IVoiture.Assembler()
        {
            Console.WriteLine("Voiture basique...");
        }
    }
}
