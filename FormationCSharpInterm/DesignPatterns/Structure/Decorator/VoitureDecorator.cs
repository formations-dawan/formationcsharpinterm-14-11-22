﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterm.DesignPatterns.Structure.Decorator
{
    internal abstract class VoitureDecorator : IVoiture
    {
        private IVoiture voiture;

        protected VoitureDecorator(IVoiture voiture)
        {
            this.voiture = voiture;
        }

        public virtual void Assembler()
        {
            voiture.Assembler();
        }
    }
}
