﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterm.DesignPatterns.Structure.Bridge
{
    internal abstract class Telecommande
    {
        protected IAppareil _appareil;

        public void DefinirAppareil(IAppareil appareil)
        {
            _appareil = appareil;
        }

        public abstract void AllumerOuEteindre();
        public abstract void ChangerVolume(int v);
        public abstract void ChangerCanal(int c);
    }
}
