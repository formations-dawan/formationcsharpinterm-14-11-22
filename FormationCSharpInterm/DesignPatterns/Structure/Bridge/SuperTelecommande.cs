﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterm.DesignPatterns.Structure.Bridge
{
    internal class SuperTelecommande : TelecommandeBasique
    {
        public void Sourdine()
        {
            ChangerVolume(0);
        }
    }
}
