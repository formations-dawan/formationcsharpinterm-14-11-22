﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterm.DesignPatterns.Structure.Bridge
{
    internal interface IAppareil
    {
        void Allumer();
        void Eteindre();
        void ChangerCanal(int c);
        void ChangerVolume(int v);
        bool EstAllume();
    }
}
