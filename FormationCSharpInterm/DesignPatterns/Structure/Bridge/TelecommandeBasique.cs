﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterm.DesignPatterns.Structure.Bridge
{
    internal class TelecommandeBasique : Telecommande
    {
        public override void AllumerOuEteindre()
        {
            if (_appareil.EstAllume())
                _appareil.Eteindre();
            else
                _appareil.Allumer();
        }

        public override void ChangerCanal(int c)
        {
            _appareil.ChangerCanal(c);
        }

        public override void ChangerVolume(int v)
        {
            _appareil.ChangerVolume(v);
        }
    }
}
