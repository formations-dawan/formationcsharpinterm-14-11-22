﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterm.DesignPatterns.Structure.Proxy
{
    //Joue le rôle d'intermédiaire pour contrôler / relayer les commandes
    internal class ProxyMessage : IMessage
    {
        private IMessage messageProxifie;

        public ProxyMessage(IMessage messageProxifie)
        {
            this.messageProxifie = messageProxifie;
        }

        string IMessage.RecupererContenu()
        {
            var contenuOriginal = messageProxifie.RecupererContenu();
            //faire ici des vérifications / transformations
            var contenuTransforme = contenuOriginal.ToUpper();
            return contenuTransforme;
        }
    }
}
