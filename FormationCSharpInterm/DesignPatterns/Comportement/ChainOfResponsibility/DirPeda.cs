﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static FormationCSharpInterm.DesignPatterns.Comportement.ChainOfResponsibility.Plainte;

namespace FormationCSharpInterm.DesignPatterns.Comportement.ChainOfResponsibility
{
    internal class DirPeda : MembreEquipe
    {
        public DirPeda(string nom, MembreEquipe successeur) : base(nom, successeur)
        {
        }

        public override void Handle(Plainte requete)
        {
            if (requete.TypePlainte == 2)
            {
                Console.WriteLine("Traitement par le directeur pédagogique.");
                requete.Etat = EtatPlainte.Ferme;
            }
            else if (successeur != null)
            {
                Console.WriteLine("Le directeur pédagogique a remonté la demande à son supérieur.");
                successeur.Handle(requete);
            }
        }
    }
}
