﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterm.DesignPatterns.Comportement.ChainOfResponsibility
{
    internal abstract class MembreEquipe //Handler (dans le schéma UML)
    {
        protected string nom;
        protected MembreEquipe successeur; //pour avoir le membre suivant dans la responsabilité

        protected MembreEquipe(string nom, MembreEquipe successeur)
        {
            this.nom = nom;
            this.successeur = successeur;
        }

        public abstract void Handle(Plainte requete);
    }
}
