﻿using System;
using static FormationCSharpInterm.DesignPatterns.Comportement.ChainOfResponsibility.Plainte;

namespace FormationCSharpInterm.DesignPatterns.Comportement.ChainOfResponsibility
{
    internal class Directeur : MembreEquipe
    {
        public Directeur(string nom, MembreEquipe successeur) : base(nom, successeur)
        {
        }

        public override void Handle(Plainte requete)
        {
            Console.WriteLine("Traitement par le directeur.");
            requete.Etat = EtatPlainte.Ferme;
        }
    }
}
