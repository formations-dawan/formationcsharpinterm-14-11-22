﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterm.DesignPatterns.Comportement.ChainOfResponsibility
{
    internal class Plainte
    {
        public int NumApprenant { get; set; }

        public int TypePlainte { get; set; }

        public string Message { get; set; }

        public EtatPlainte Etat { get; set; }

        public enum EtatPlainte
        {
            Ouvert,
            Ferme
        }

        public Plainte(int numApprenant, int typePlainte, string message, EtatPlainte etat)
        {
            NumApprenant = numApprenant;
            TypePlainte = typePlainte;
            Message = message;
            Etat = etat;
        }
    }
}
