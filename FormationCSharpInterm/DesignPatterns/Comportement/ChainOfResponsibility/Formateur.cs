﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Lifetime;
using System.Text;
using System.Threading.Tasks;
using static FormationCSharpInterm.DesignPatterns.Comportement.ChainOfResponsibility.Plainte;

namespace FormationCSharpInterm.DesignPatterns.Comportement.ChainOfResponsibility
{
    internal class Formateur : MembreEquipe
    {
        public Formateur(string nom, MembreEquipe successeur) : base(nom, successeur)
        {
        }

        public override void Handle(Plainte requete)
        {
            if (requete.TypePlainte == 1)
            {
                Console.WriteLine("Traitement par le formateur.");
                requete.Etat = EtatPlainte.Ferme;
            }
            else if (successeur != null)
            {
                Console.WriteLine("Le formateur a remonté la demande à son supérieur.");
                successeur.Handle(requete);
            }
        }
    }
}
