﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterm.DesignPatterns.Comportement.Command
{
    internal class WaiterInvoker
    {
        public void TakeOrder(IOrderCommand order)
        {
            order.Execute();
        }
    }
}
