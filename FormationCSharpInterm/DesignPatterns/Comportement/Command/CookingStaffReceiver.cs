﻿namespace FormationCSharpInterm.DesignPatterns.Comportement.Command
{
    internal class CookingStaffReceiver
    {
        public void CookLunch(string orderContent)
        {
            System.Console.WriteLine("Repas du midi : " + orderContent);
        }

        public void CookDinner(string orderContent)
        {
            System.Console.WriteLine("Repas du soir : " + orderContent);
        }
    }
}