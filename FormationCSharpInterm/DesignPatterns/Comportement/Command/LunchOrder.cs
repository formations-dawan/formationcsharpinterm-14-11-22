﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Channels;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterm.DesignPatterns.Comportement.Command
{
    internal class LunchOrder : IOrderCommand
    {
        protected readonly CookingStaffReceiver receiver;

        public string OrderContent { get; set; }

        public enum OrderState { Created, Prepared };
        public OrderState State { get; set; }

        public LunchOrder(string orderContent, CookingStaffReceiver receiver)
        {
            this.receiver = receiver;
            OrderContent = orderContent;
            State = OrderState.Created;
        }

        public virtual void Execute()
        {
            receiver.CookLunch(OrderContent);
            State = OrderState.Prepared;
        }
    }
}
