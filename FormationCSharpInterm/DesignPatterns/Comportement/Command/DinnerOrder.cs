﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterm.DesignPatterns.Comportement.Command
{
    internal class DinnerOrder : LunchOrder //Héritage exceptionnel car les commandes ont les mêmes détails (état)
    {
        public DinnerOrder(string orderContent, CookingStaffReceiver receiver) : base(orderContent, receiver)
        {
        }

        public override void Execute()
        {
            receiver.CookDinner(OrderContent);
            State = OrderState.Prepared;
        }
    }
}
