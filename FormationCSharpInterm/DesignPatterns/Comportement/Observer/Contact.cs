﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterm.DesignPatterns.Comportement.Observer
{
    internal class Contact : IObserver<EvenementChangementPrix>
    {
        public string Nom { get; set; }

        public void OnCompleted()
        {
            Debug.WriteLine("Termine...");
        }

        public void OnError(Exception error)
        {
            Debug.WriteLine(error.Message);
        }

        public void OnNext(EvenementChangementPrix value)
        {
            Console.WriteLine(value.Date.ToString("dd/MM/yy") + " : " + value.MessageNotification);
        }
    }
}
