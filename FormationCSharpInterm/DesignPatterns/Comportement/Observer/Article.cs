﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterm.DesignPatterns.Comportement.Observer
{
    internal class Article : IObservable<EvenementChangementPrix>
    {
        public string Description { get; set; }

        private List<IObserver<EvenementChangementPrix>> observateurs;

        public Article()
        {
            observateurs = new List<IObserver<EvenementChangementPrix>>();
        }

        public IDisposable Subscribe(IObserver<EvenementChangementPrix> observer)
        {
            if (!observateurs.Contains(observer))
                observateurs.Add(observer);

            return new Desabonnement(observateurs, observer); //Unsubscriber
        }

        private double _prix;

        public double Prix
        {
            get => _prix;
            set
            {
                _prix = value;

                EvenementChangementPrix e = new EvenementChangementPrix(DateTime.Now,
                    "Changement de prix pour " + Description + " : " + Prix);

                foreach (IObserver<EvenementChangementPrix> obs in observateurs)
                {
                    obs.OnNext(e);
                }
            }
        }
    }
}
