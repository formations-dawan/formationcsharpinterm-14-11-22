﻿using System;

namespace FormationCSharpInterm.DesignPatterns.Comportement.Observer
{
    internal class EvenementChangementPrix
    {
        public DateTime Date { get; set; }

        public string MessageNotification { get; set; }

        public EvenementChangementPrix(DateTime date, string messageNotification)
        {
            Date = date;
            MessageNotification = messageNotification;
        }
    }
}