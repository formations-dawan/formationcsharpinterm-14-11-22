﻿using System;
using System.Collections.Generic;

namespace FormationCSharpInterm.DesignPatterns.Comportement.Observer
{
    internal class Desabonnement : IDisposable
    {
        private List<IObserver<EvenementChangementPrix>> observateurs;
        private IObserver<EvenementChangementPrix> observer;

        public Desabonnement(List<IObserver<EvenementChangementPrix>> observateurs, IObserver<EvenementChangementPrix> observer)
        {
            this.observateurs = observateurs;
            this.observer = observer;
        }

        public void Dispose()
        {
            if (observateurs != null)
                observateurs.Remove(observer);
        }
    }
}