﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterm.Polymorphisme
{
    internal class PolyTest
    {
        // ad-hoc : niveau de fonction avec GetType
        public static void Buy(Object obj)
        {
            if (obj.GetType().Equals(typeof(Table)))
            {
                var table = (Table)obj;
                //...
            }
        }

        // sous-typage : implémente une interface / hérite d'une classe
        public static void Buy(IPliable pliable)
        {
            // pas besoin de tester le type à chaque fois
            pliable.Plier();
        }

        // types paramétriques (génériques)
        public static void Buy<T>(T pliable) where T : IPliable
        {
            // pas besoin de tester le type à chaque fois
            pliable.Plier();
        }
    }
}
