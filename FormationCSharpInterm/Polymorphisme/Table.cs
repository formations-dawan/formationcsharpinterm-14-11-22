﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterm.Polymorphisme
{
    internal class Table : IPliable
    {
        public string Deplier()
        {
            return "Déplier...";
        }

        public string Plier()
        {
            return "Plier...";
        }
    }
}
