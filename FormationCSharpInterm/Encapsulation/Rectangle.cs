﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterm.Encapsulation
{
    internal class Rectangle
    {
        #region Champs et proprietes
        private double _longueur;
        private double _largeur;

        private double Longueur
        {
            get => _longueur;
            set
            {
                if (value < 0) throw new ArgumentOutOfRangeException("La longueur ne pas être négative !");
                _longueur = value;
            }
        }
        private double Largeur 
        {
            get => _largeur;
            set 
            {
                if (value < 0) throw new ArgumentOutOfRangeException("La largeur ne pas être négative !");
                _largeur = value;
            }
        }
        #endregion

        #region Constructeur
        public Rectangle(double longueur, double largeur)
        {
            Redim(longueur, largeur);
        }
        #endregion

        #region Methodes
        public void Redim(double longueur, double largeur)
        {
            Longueur = longueur;
            Largeur = largeur;
        }

        public double Aire
        {
            get => Longueur * Largeur;
        }
        #endregion
    }
}
