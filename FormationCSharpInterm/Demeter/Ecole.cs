﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterm.Demeter
{
    internal class Ecole
    {
        public IList<Niveau> Niveaux { get; set; }

        //Cette méthode ne respecte pas la loi de Démeter
        /*public int CompterEtudiants()
        {
            int nb = 0;

            foreach (Niveau n in Niveaux)
                foreach (ClasseEtudiants c in n.ClassesEtudiants)
                    foreach (Etudiant e in c.Etudiants)
                        nb++;
            return nb;
        }*/

        public int CompterEtudiants()
        {
            int nb = 0;

            foreach (var n in Niveaux)
            {
                nb += n.CompterEtudiants();
            }

            return nb;
        }
    }
}
