﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterm.Demeter
{
    internal class Niveau
    {
        public IList<ClasseEtudiants> ClassesEtudiants { get; set; }

        public int CompterEtudiants()
        {
            int nb = 0;

            foreach (var classeEtudiants in ClassesEtudiants)
            {
                nb += classeEtudiants.CompterEtudiants();
            }

            return nb;
        }
    }
}
