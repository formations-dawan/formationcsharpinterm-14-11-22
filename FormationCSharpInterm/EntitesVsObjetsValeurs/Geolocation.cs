﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterm.EntitesVsObjetsValeurs
{
    //ValueObject : objet immutable (renseigné à la création), valeur ne bouge pas
    internal class Geolocation : IComparable<Geolocation>
    {
        private double _latitude;
        private double _longitude;

        public Geolocation(double latitude, double longitude)
        {
            _latitude = latitude;
            _longitude = longitude;
        }

        public override bool Equals(object obj)
        {
            return obj is Geolocation geo && 
                _latitude == geo._latitude && 
                _longitude == geo._longitude;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return $"{_longitude}, {_latitude}";
        }
        public int CompareTo(Geolocation other)
        {
            //Comparaison basée sur la distance

            double rayonTerre = 6371; //en km

            double dLat = ConversionEnRadians(other._latitude - _latitude);
            double dLng = ConversionEnRadians(other._longitude - _longitude);

            double sindLat = Math.Asin(dLat / 2);
            double sindLng = Math.Asin(dLng / 2);

            double a = Math.Pow(sindLat, 2) + Math.Pow(sindLng, 2)
                * Math.Cos(ConversionEnRadians(_latitude)) * Math.Cos(ConversionEnRadians(other._latitude));

            double c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));

            double dist = c * rayonTerre;

            return Convert.ToInt32(dist);
        }

        private static double ConversionEnRadians(double angle)
        {
            return Math.PI / 180 * angle;
        }
    }
}
