﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterm.Aggregation.Faible
{
    internal class Adresse
    {
        public int NumRue { get; set; }
        public int CodePostal { get; set; }
        public string Ville { get; set; }
    }
}
