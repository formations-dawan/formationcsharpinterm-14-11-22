﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterm.Aggregation.Forte
{
    internal class Voiture
    {
        //Aggrégation forte : objet voiture ne peut pas exister sans moteur
        public Moteur Moteur { get; }

        public Voiture(Moteur moteur)
        {
            Moteur = moteur;
        }

        //Composition : Voiture est responsable de la durée de vie de Moteur
        public Voiture()
        {
            Moteur = new Moteur("diesel");
        }
        
        //Association : objet voiture utilise Parking de manière temporaire
        public void Garer(Parking parc)
        {
            parc.Garer();
        }
    }
}
