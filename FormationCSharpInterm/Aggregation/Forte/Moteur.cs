﻿namespace FormationCSharpInterm.Aggregation.Forte
{
    public class Moteur
    {
        public string Motorisation { get; }

        public Moteur(string motorisation)
        {
            Motorisation = motorisation;
        }
    }
}