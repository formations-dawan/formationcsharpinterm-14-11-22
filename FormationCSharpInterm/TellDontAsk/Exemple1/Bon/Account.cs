﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterm.TellDontAsk.Exemple1.Bon
{
    internal class Account
    {
        public int Id { get; set; }
        public double Balance { get; set; }

        public Account(int id, double balance)
        {
            Id = id;
            Balance = balance;
        }

        public void Withdraw(double amount)
        {
            if (Balance < amount)
                throw new ArgumentException("Pas assez d'argent");

            Balance -= amount;
        }
    }
}
