﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterm.TellDontAsk.Exemple1.Bon
{
    internal interface IAccountRepository
    {
        Account FindById(int accountId);
        void Save(Account account);
    }
}
