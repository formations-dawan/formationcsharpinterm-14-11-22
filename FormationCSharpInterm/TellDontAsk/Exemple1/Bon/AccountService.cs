﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterm.TellDontAsk.Exemple1.Bon
{
    internal class AccountService : IAccountService
    {
        private readonly IAccountRepository _accountRepository;

        public AccountService(IAccountRepository accountRepository)
        {
            _accountRepository = accountRepository;
        }

        void IAccountService.Withdraw(int accountId, double amount)
        {
            var account = _accountRepository.FindById(accountId);
            account.Withdraw(amount);
            _accountRepository.Save(account);
        }
    }
}
