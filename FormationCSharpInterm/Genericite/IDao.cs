﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterm.Genericite
{
    internal interface IDao<TEntite>
    {
        List<TEntite> RecupererTout();

        int Inserer(TEntite obj);
    }
}
