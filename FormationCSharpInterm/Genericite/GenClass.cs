﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterm.Genericite
{
    internal class GenClass<T>  //where T : new() : doit avoir un constructeur public
                                //where T : Produit : doit hériter de Produit
    {
        public T A { get; set; }
        public T B { get; set; }

        public void Swap()
        {
            T c = A;
            A = B;
            B = c;
        }
    }
}
