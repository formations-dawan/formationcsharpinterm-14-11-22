﻿namespace FormationCSharpInterm.Genericite
{
    public class Produit
    {
        public string Description { get; set; }
        public double Prix { get; set; }
    }
}