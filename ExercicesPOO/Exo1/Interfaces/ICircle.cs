﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercice1
{
    internal interface ICircle
    {
        double getArea();
        double getPerimeter();
    }
}
