﻿using System;

namespace ExercicesPOO.Exo3
{
    public class Complex
    {
        public double a { get; set; }
        public double b { get; set; }

        public Complex(double real, double imaginaire)
        {
            a = real;
            b = imaginaire;
        }
        public override string ToString()
        {
            return "(" + a + "," + b + ")";
        }
        public double GetMagnitude()
        {
            return Math.Sqrt((a * a) + (b * b));
        }
        public void Sum(Complex c)
        {
            a += c.a;
            b += c.b;
        }


        /*
          public Complex Sum (Complex complex)
        {
            Complex complex2 = new Complex(0, 0);
            complex2.Reelle = Reelle + complex.Reelle;
            complex2.Imaginaire = Imaginaire + complex.Imaginaire;

            return complex2;    
        }
         

        methode greg
        public Complex Sum (Complex complex) 
        {
            return new Complex(this.real + complex.real, 
                this.imaginary + complex.imaginary);
        }
         */
    }
}
