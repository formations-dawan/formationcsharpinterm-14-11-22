﻿using System;

namespace Exercice2
{
    public class Person
    {
        protected int age;

        public void SetAge(int n)
        {
            age = n;
        }

        public void SayHello()
        {
            Console.WriteLine("Hello");
        }
    }
}