﻿using System;

namespace Exercice2
{
    public class Student : Person
    {
        public void GoToClasses()
        {
            Console.WriteLine("I'm going to class.");
        }

        public void DisplayAge()
        {
            Console.WriteLine("My age is: " + age + " years old");
        }
    }
}