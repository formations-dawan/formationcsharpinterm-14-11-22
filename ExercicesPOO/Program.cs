﻿using Exercice1;
using Exercice2;
using ExercicesPOO.Exo3;
using Exo4;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Exo4Person = Exo4.Person;

namespace ExercicesPOO
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            //Exo 1
            Console.WriteLine("---------------EXERCICE 1----------------");
            ICircle circle = new Circle(6.25);
            Console.WriteLine("Area: " + circle.getArea());
            Console.WriteLine("Perimeter: " + circle.getPerimeter());

            //Exo 2
            Console.WriteLine("---------------EXERCICE 2----------------");

            Exercice2.Person person = new Exercice2.Person();
            person.SayHello();

            Student student = new Student();
            student.SetAge(15);
            student.SayHello();
            student.GoToClasses();
            student.DisplayAge();

            Teacher teacher = new Teacher();
            teacher.SetAge(40);
            teacher.SayHello();
            teacher.Explain();

            //Exo 3
            Console.WriteLine("---------------EXERCICE 3----------------");
            Complex cm = new Complex(4,8);
            Console.WriteLine("le nombre est :" + cm.ToString());
            cm.b = 6;
            Console.WriteLine("le nombre est :" + cm.ToString());

            Console.WriteLine("l'ordre de grandeur :" + cm.GetMagnitude());

            Complex cm1 = new Complex(-2,3);
            cm.Sum(cm1);
            Console.WriteLine("Aprés l'addition: " + cm.ToString());

            //Exo 4
            Console.WriteLine("---------------EXERCICE 4----------------");
            Apartment a = new Apartment();
            Exo4Person per = new Exo4Person();

            per.Name = "Antoine";
            per.House = a;
            per.Display();

            Console.ReadKey();
        }
    }
}
