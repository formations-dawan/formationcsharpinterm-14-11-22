﻿using System;

namespace Exo4
{
    public class Door
    {

        protected string color;
        public Door()
        {
            color = "blue";
        }

        public Door(string color)
        {
            this.color = color;
        }

        public string Color
        {
            get { return color; }
            set { color = value; }
        }

        public void Display()
        {
            Console.WriteLine("Je suis une porte, ma couleur est " + color);
        }
    }
}