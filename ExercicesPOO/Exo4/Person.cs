﻿using System;
namespace Exo4
{
    public class Person
    {
        protected string name;
        protected House house;

        public Person()
        {
            name = "Soukaina";
            house = new House(150);
        }

        public Person(string name, House house)
        {
            this.name = name;
            this.house = house;
        }
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public House House
        {
            get { return house; }
            set { house = value; }
        }
        public void Display()
        {
            Console.WriteLine("Je m'appele " + name);
            house.Display();
        }
    }
}

