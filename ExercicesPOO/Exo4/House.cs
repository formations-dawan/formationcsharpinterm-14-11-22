﻿using System;

namespace Exo4
{
    public class House
    {
        protected int surface;
        protected Door door;

        public House(int surface)
        {
            this.surface = surface;
            door = new Door();
        }

        public int Surface
        {
            get { return surface; }
            set { surface = value; }
        }

        public Door Door
        {
            get { return door; }
            set { door = value; }
        }
        public virtual void Display()
        {
            Console.WriteLine("Je suis une maison, ma surface est de " + surface, "m2");
            door.Display();
        }
    }
 }