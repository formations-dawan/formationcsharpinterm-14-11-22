﻿using System;

namespace Exo4
{
    public class Apartment : House
    {
        public Apartment() : base(50)
        {
        }

        public Apartment(int surface) : base(surface)
        {
        }

        public override void Display()
        {
            Console.WriteLine("Je suis un appartement, ma surface est " + surface + " m2");
        }
    }
}